# BO-ECLI WS-2

BO-ECLI WS-2 is a project that creates software for finding legal references in texts.

The software exposes a REST API to which text can be sent. The text is annotated text is returned as XML.

# New Feature

Resolve links with Watson.

# Building

The code can be built with maven:
```bash
mvn install
```

To update the dependencies, run:
```bash
mvn versions:display-dependency-updates
```

# Testing

Start the service:

```bash
cd engine
mvn install && java -jar target/*.jar
```

Send a test to the service:

```bash
$ cat > test.xml
<bo:engine-input xmlns:bo="http://boecli.eu/engine/0.1">
  <p>hello <span>world</span>.</p>
</bo:engine-input>
$ curl -v -H "Accept: text/xml" -H "Content-Type: text/xml" --data @test.xml http://localhost:8080/entry-point/run
```
