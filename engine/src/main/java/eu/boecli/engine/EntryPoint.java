package eu.boecli.engine;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;
import org.w3c.dom.Element;

import eu.boecli.engine.schema.EngineInput;
import eu.boecli.engine.schema.EngineOutput;
import eu.boecli.engine.schema.Jurisdiction;
import eu.boecli.engine.schema.Jurisdictions;
import eu.boecli.engine.schema.Language;
import eu.boecli.engine.schema.Languages;
import eu.boecli.engine.schema.ObjectFactory;

@Path("/entry-point")
public class EntryPoint {

    @GET
    @Path("languages")
    @Produces(MediaType.TEXT_XML)
    public JAXBElement<Languages> languages() {
        Languages l = new Languages();
        l.getLanguage().add(Language.NL);
        return new ObjectFactory().createLanguages(l);
    }

    @GET
    @Path("jurisdictions")
    @Produces(MediaType.TEXT_XML)
    public JAXBElement<Jurisdictions> jurisdictions() {
        Jurisdictions j = new Jurisdictions();
        j.getJurisdiction().add(Jurisdiction.NL);
        return new ObjectFactory().createJurisdictions(j);
    }

    @POST
    @Path("run")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public JAXBElement<EngineOutput> run(@Valid EngineInput input) {
        Element document = (Element)input.getAny();
        EngineOutput output = new EngineOutput();
        output.setAny(document);
        return new ObjectFactory().createEngineOutput(output);
    }
}
